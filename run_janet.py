#!/usr/bin/env python
# I don't prefer using env but in this case
# it makes it flexible to use with or without a virtualenv

### THE ORDER OF THESE ONES MATTER ###
######################################

import gevent.monkey

gevent.monkey.patch_all()

import settings

import sentry_sdk

sentry_sdk.init(environment=settings.SENTRY_ENV)

######################################
### ### ##### ## ##### #### ###### ###

import time
import gevent
import random

from JanetWorker import JanetWorker
from JanetNeuron import JanetNeuron, StaticJanetNeuron

import zmq.green as zmq


class Q:
    def __init__(self, zmq_context):
        self.zmq_context = zmq_context
        self.frontend = self.zmq_context.socket(zmq.PULL)
        self.frontend.bind("ipc:///tmp/janet_q_in")

        self.backend = self.zmq_context.socket(zmq.PUSH)
        self.backend.bind("ipc:///tmp/janet_q_out")

    def run(self):
        zmq.device(zmq.STREAMER, self.frontend, self.backend)


if __name__ == "__main__":
    listen_janet = StaticJanetNeuron()

    zmq_context = zmq.Context()
    queue_device = Q(zmq_context)
    gevent.spawn(queue_device.run)

    # Make a bunch of workers to handle incoming
    # messages. Or whatever. Put a thing in the queue
    # and Janet will try to deal with it
    for i in range(settings.worker_count):
        j = JanetWorker(zmq_context)
        gevent.spawn(j)

    q_send_sock = zmq_context.socket(zmq.PUSH)
    q_send_sock.connect("ipc:///tmp/janet_q_in")

    if listen_janet.client.rtm_connect():
        while listen_janet.client.server.connected is True:
            for line in listen_janet.client.rtm_read():
                backoff_timer = 0
                while True:
                    backoff_timer += 1
                    try:
                        q_send_sock.send_json(line)
                        break
                    except gevent.queue.Full:
                        raise
                        gevent.sleep(2 ** backoff_timer)
            gevent.sleep(settings.mainloop_delay)
    else:
        print("Connection Failed")
