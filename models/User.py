# internal
from models.Sender import Sender
from JanetNeuron import JanetNeuron

# stdlib
import datetime
import re
import sys

# database
from pynamodb.attributes import UnicodeAttribute, JSONAttribute, UTCDateTimeAttribute
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from pynamodb.models import Model


class DisplayNameIndex(GlobalSecondaryIndex):
    class Meta:
        projection = AllProjection()
        read_capacity_units = 1
        write_capacity_units = 1

    display_name = UnicodeAttribute(hash_key=True)


# Sender inherits from Neuron so you've got all that functionality for free if you want it
class User(Model, Sender):
    class Meta:
        table_name = f"{JanetNeuron().workspace_name}_users"

    uid = UnicodeAttribute(hash_key=True)
    display_name_index = DisplayNameIndex()
    display_name = UnicodeAttribute()
    created_date = UTCDateTimeAttribute(default=datetime.datetime.utcnow)

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     if not self.user_info_from_id(self.uid):
    #         raise ValueError(f"No user for {self.uid} found via slack API")

    @classmethod
    def from_source_object(cls, source_obj, create=False):
        if hasattr(source_obj, "raw_line"):
            source_obj = source_obj.raw_line

        user = None

        if "id" in source_obj:  # from user object
            user = source_obj
            source_uid = source_obj["id"]
        elif "user" in source_obj:  # from message event
            source_uid = source_obj["user"]
            try:
                iter(source_uid)
                source_uid = source_uid["id"]
            except:
                ...
        elif "bot_id" in source_obj:  # from bot message event
            try:
                source_uid = source_obj["user"]
            except KeyError:
                source_uid = source_obj["bot_id"]
        else:
            raise ValueError(f"Could not construct Sender from {source_obj}")
        user_instance = cls.from_uid(source_uid, create=create)
        user_instance.source_obj = source_obj
        if user:
            user_instance._user = user
        if "_profile" in source_obj:
            user_instance._display_name = source_obj["profile"][
                "display_name_normalized"
            ].lower()
            user_instance.save()
        return user_instance

    # just an alias so I don't have to remember if I shortened it or not
    from_source_obj = from_source_object

    # Returns a User instance or None. Should probably raise instead of None? idk
    # for now fail_on_create will raise an exception if you try to get a user obj
    # for a uid that doesn't already exist
    @classmethod
    def from_uid(cls, uid, create=False):
        uid = cls.normalize_uid(uid)

        try:
            user = cls.get(uid)
            return user
        except cls.DoesNotExist:
            if not create:
                return

        display_name = JanetNeuron().userid_to_display_name(uid)
        user = cls(uid=uid, display_name=display_name)
        user.save()
        return user

        # Hmmmmm
        # All the from_ methods need to make their own session
        # Could possibly make it a static method?

    @classmethod
    def from_display_name(cls, display_name):
        display_name = display_name.lower()
        maybe_users = [i for i in cls.display_name_index.query(display_name)]
        # uh oh if two people have the same name this could be bad
        # nobody tell bng
        if maybe_users:
            return maybe_users[0]

    def validate_uid_field(self, key, uid):
        uid = self.normalize_uid(uid)
        return uid

    @classmethod
    def normalize_uid(cls, uid):
        display_uid_pattern = r"<@([A-Za-z0-9]*?)>"
        if re.match(display_uid_pattern, uid):
            uid = re.sub(display_uid_pattern, r"\1", uid)
        return uid.upper()

    @property
    def at_name(self):
        return f"<@{self.uid.upper()}>"

    def __repr__(self):
        return f"{self.display_name} -> {self.uid} ({self.at_name})"


if not User.exists():
    User.create_table(read_capacity_units=1, write_capacity_units=1)
