import requests
from requests.exceptions import HTTPError, ConnectionError
from botocore.exceptions import NoCredentialsError
import os
import time
import hashlib

on_aws = False
no_secrets_available = False

try:
    aws_local_response = requests.get("http://169.254.170.2/v2/metadata", timeout=1)
    on_aws = aws_local_response.ok
except (ConnectionError, HTTPError):
    pass

try:
    from .settings_aws import *
except (NoCredentialsError) as e:
    no_secrets_available = True
except (KeyError) as e:
    no_secrets_available = True

if not on_aws or no_secrets_available:
    from .settings_env import *

    if all(slack_tokens):
        print("SECRETS FROM ENV")
else:
    print("SECRETS FROM AWS")

token_string = slack_tokens["bot"] + slack_tokens["user"]
token_string = bytes(token_string, "utf-8")
session_id = hashlib.md5(token_string).hexdigest()[:7]

event_queue_socket_path = "ipc://janet_queue_socket"
q_sock_path = event_queue_socket_path
