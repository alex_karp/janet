#!/usr/bin/env python
# This script sends a ?ping message and waits for a pong.
# Just a basic health check

import time
import sys

import sentry_sdk

sentry_sdk.init(environment="test_suite")

max_check_attempts = 5
check_counter = 0

from JanetNeuron import JanetNeuron, StaticJanetNeuron
from log.metrics import Metric

if __name__ == "__main__":
    listen_janet = StaticJanetNeuron()

    begin_time = time.time()
    if listen_janet.client.rtm_connect():
        print("preparing to ping...")
        listen_janet.send_message("?ping")
        print("ping sent")
        line = listen_janet.client.rtm_read()
        while line and check_counter <= max_check_attempts:
            check_counter += 1
            print("reading from RTM")
            for line in listen_janet.client.rtm_read():
                print(f"read: {line}")
                if "text" in line:
                    if line["text"] == "pong":
                        print("healthy")
                        end_time = time.time()
                        event_processing_time = end_time - begin_time
                        Metric("health_check_time", value=event_processing_time).put()
                        sys.exit(0)
            print("no pong in response, sleeping and retrying")
            time.sleep(0.5)
    else:
        print("Connection Failed")

end_time = time.time()
event_processing_time = end_time - begin_time
Metric("health_check_time", value=event_processing_time).put()
print("unhealthy")
sys.exit(1)
