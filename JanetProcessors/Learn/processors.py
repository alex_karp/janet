from sqlalchemy.sql.expression import func, select
from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import (
    messages_only,
    no_bots,
    i_startswith_words,
    on_reacts,
)
from events.Message import Message
from events.ReactionAdded import ReactionAdded
from sqlalchemy import or_
from . import models
import random
import re

from models.User import User
from log.metrics import Metric


class Learn(JanetProcessor):
    learn_words = ("?learn ", "?pluslearn")
    learn_emoji = ("learn", "pluslearn", "lantern")

    unlearn_words = ("?unlearn ",)
    unlearn_emoji = ("unlearn",)

    get_learns_words = ("?learn?", "gimme", "?gimme")

    learn_list_words = ("?learns ", "?list ")

    learnsearch_words = ("?learnsearch ",)

    @i_startswith_words(*learn_words)
    def process_learn_words(self):
        self.learn()

    @on_reacts(*learn_emoji)
    def process_learn_reacts(self):
        self.learn()

    @i_startswith_words(*learnsearch_words)
    def process_learnsearch_words(self):
        self.learnsearch()

    @i_startswith_words(*unlearn_words)
    def process_unlearn_words(self):
        self.unlearn()

    @on_reacts(*unlearn_emoji)
    def process_unlearn_reacts(self):
        self.unlearn()

    @i_startswith_words(*get_learns_words)
    def process_all_gets(self):
        self.recall()

    @i_startswith_words(*learn_list_words)
    def process_list_learns(self):
        self.list()

    @property
    def _list(self):
        learn_target = self.learn_target

        if learn_target == "*":
            # when will this OOM? yolo
            learns = models.Learn.scan(limit=50000)
        else:
            learns = models.Learn.query(learn_target)

        return [i for i in learns]

    def list(self):
        learns = self._list
        learns = "\n".join(
            [
                f"{learn.text}\n>learned by: {learn.learned_by.at_name}"
                for learn in learns
            ]
        )
        self.reply_dm(learns)

    def normalize_string(self, string):
        return re.sub(r"[\W\s]", " ", string).lower()

    # DaTa ScIeNcE
    # https://towardsdatascience.com/overview-of-text-similarity-metrics-3397c4601f50
    def jaccard_similarity(self, these_words, those_words):
        a = set(self.normalize_string(these_words).split())
        b = set(self.normalize_string(those_words).split())
        c = a.intersection(b)

        try:
            similarity = float(len(c)) / (len(a) + len(b) - len(c))
        except ZeroDivisionError:
            similarity = 0.0
        return similarity

    def learnsearch(self):
        search_results = []
        search_text = self.learn_text.lower()

        for learn in self._list:
            similarity = self.jaccard_similarity(
                search_text.lower(), learn.text.lower()
            )
            if similarity:
                search_results.append(learn)
            elif search_text in learn.text.lower():
                search_results.append(learn)

        for learn in search_results:
            self.event.reply_thread(learn)

        return search_results

    def learn(self):
        if not (self.learn_target and self.learn_text):
            self.reply(self.random_polite_refusal)
            return

        # no exact duplicates allowed for learn
        old_learns = models.Learn.query(
            self.learn_target, models.Learn.text == self.learn_text
        )
        old_learns = [i for i in old_learns]

        if any(old_learns):
            # user already has this learn. noop.
            return

        new_learn = models.Learn(self.learn_target, text=self.learn_text)
        new_learn._learned_by = self.event.sender.uid
        new_learn.save()
        Metric("learns_created", value=1).put()

        self.event.reply_thread(f"OK, learned {self.learn_target}")

    def recall(self):
        if self.cmd.replace("?", "").isdigit():
            learn_num = self.cmd.replace("?", "")
            # recalled_learn = session.query(models.Learn).filter(models.Learn.pk == learn_num).first()
            # idk what to do yet here
        elif self.cmd:
            recalled_learn = [i for i in models.Learn.query(self.learn_target)]
            if any(recalled_learn):
                recalled_learn = random.choice([i for i in recalled_learn])

        if recalled_learn:
            self.reply(recalled_learn.text)

    def unlearn(self):
        if not (self.learn_target and self.learn_text):
            self.reply(self.random_polite_refusal)
            return

        old_learns = models.Learn.query(
            self.learn_target, models.Learn.text == self.learn_text
        )
        old_learns = [i for i in old_learns]
        if len(old_learns) < 1:
            # user doesn't have this learn. noop.
            self.reply("That learn doesn't exist")
            return
        elif len(old_learns) > 1:
            self.reply("That learn exists more than once. I'm gonna delete one of them")

        learn = old_learns[0]
        learn.delete()
        self.reply(f"Unlearned {self.learn_target}!")

    @property
    def learn_target(self):
        if type(self.event) == ReactionAdded:
            return self.event.reacted_to_user.at_name

        if self.args:
            candidate = self.args[0]
        elif len(self.split_text):
            candidate = self.split_text[0][1:]  # trim off the question mark
        candidate = candidate.lower()

        if candidate == "*":
            return "*"

        maybe_uid = User.normalize_uid(candidate)

        try:
            potential_user = User.get(maybe_uid)
        except:
            potential_user = User.display_name_index.count(candidate)
            if potential_user:
                potential_user = User.display_name_index.query(candidate).next()

        if potential_user:
            candidate = potential_user.at_name

        return candidate

    @property
    def learn_text(self):
        if len(self.split_text) >= 3:
            return " ".join(self.split_text[2:])
        elif type(self.event) == ReactionAdded:
            return self.event.message.text
