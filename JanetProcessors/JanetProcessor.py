import inspect
import pprint
import shlex
import argparse
from unidecode import unidecode
from events.Message import Message
from JanetNeuron import JanetNeuron


class JanetProcessor(JanetNeuron):
    def __init__(self, event):
        self.event = event

    def reply(self, message):
        self.event.reply(message)

    def reply_dm(self, message):
        self.event.reply_dm(message)

    dm_reply = reply_dm

    @property
    def split_text(self):
        if not hasattr(self.event, "text"):
            return ""

        text = unidecode(self.event.text)
        return shlex.split(text, posix=False)

    @property
    def args(self):
        return self.split_text[1:]

    @property
    def cmd(self):
        return self.split_text[0]

    command = cmd

    def process(self):
        return
        self.send_message("hi there, I'm Janet")

    def check_is_entry_point(self, attrname):
        try:
            attr_class = getattr(self.__class__, attrname)
            attr_class.JANET_ENTRY

        except AttributeError:
            return False

        return True

    @property
    def entry_points(self):
        if not hasattr(self, "_entries"):
            self._entries = []

        if not self._entries:
            self._entries = [
                getattr(self, attr) for attr in dir(self) if self.check_is_entry_point(attr)
            ]

        return self._entries
