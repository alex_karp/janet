from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import i_startswith_words

from models.User import User


class Poop(JanetProcessor):
    poop_words = ("?poop",)
    template = """:poop::poop::poop: {username} has been pooped! lock your laptops, folks. :poop::poop::poop:"""

    @i_startswith_words(*poop_words)
    def process(self):
        response_text = self._response_text()
        self.reply(response_text)
        return response_text

    def _response_text(self):
        return self.template.format(username=self.event.sender.display_name)
