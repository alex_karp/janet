from events.Message import Message
from events.ReactionAdded import ReactionAdded
from events.Hello import Hello
from events.UserChange import UserChange

import random

# All this decorator does is add the JANET_ENTRY
# attribute to the function object.
# This lets us loop over all the attributes
# of the processor and find ones with a JANET_ENTRY
class basic_func_wrapper:
    JANET_ENTRY = True

    def __init__(self, func, *args, **kwargs):
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def __call__(self, processor):
        return self.func(processor)

    def __repr__(self):
        return self.func.__repr__()


class no_bot_func_wrapper(basic_func_wrapper):
    def __call__(self, processor):
        if not processor.event.sender.is_bot:
            return self.func(processor)


# takes as an argument the rate at which to sample events
# i.e. if you put in .5, half of all events will be processed
# if you put in .1, 10% of events will be processed.
def sampled_at(sample_rate):
    s_rate = sample_rate  # sometimes I don't understand scope

    class inner_wrapper(basic_func_wrapper):
        sample_rate = int(s_rate * 100)

        trues = [True] * (sample_rate)
        falses = [False] * (100 - sample_rate)

        def __call__(self, processor):
            do_it = random.choice(self.falses + self.trues)
            if do_it:
                return self.func(processor)

    return inner_wrapper


def typed_func_wrapper(*accepted_types):
    a_types = accepted_types  # sometimes I don't understand scope

    class inner_wrapper(basic_func_wrapper):
        accepted_types = a_types

        def __call__(self, processor):
            if type(processor.event) in accepted_types:
                return self.func(processor)

    return inner_wrapper


def startswith_words(*words):
    w = words

    class inner_wrapper(basic_func_wrapper):
        words = tuple(w)

        def __call__(self, processor):
            if not type(processor.event) == Message:
                return
            if processor.event.text.startswith(self.words):
                return self.func(processor)

    return inner_wrapper


def i_startswith_words(*words):
    w = [word.lower() for word in words]

    class inner_wrapper(basic_func_wrapper):
        words = tuple(w)

        def __call__(self, processor):
            if not type(processor.event) == Message:
                return
            if processor.event.text.lower().startswith(self.words):
                return self.func(processor)

    return inner_wrapper


def contains_words(*words):
    w = words

    class inner_wrapper(basic_func_wrapper):
        words = tuple(w)

        def __call__(self, processor):
            if not type(processor.event) == Message:
                return

            message_text = processor.event.text
            if any([word in message_text for word in self.words]):
                return self.func(processor)

    return inner_wrapper


def i_contains_words(*words):
    w = [word.lower() for word in words]

    class inner_wrapper(basic_func_wrapper):
        words = tuple(w)

        def __call__(self, processor):
            # I wanna inherit from typed_func_wrapper or something
            # so I don't have to do write out the type check
            if not type(processor.event) == Message:
                return

            message_text = processor.event.text.lower()
            if any([word in message_text for word in self.words]):
                return self.func(processor)

    return inner_wrapper


def on_reacts(*emoji):
    e = emoji

    class inner_wrapper(basic_func_wrapper):
        emoji = e

        def __call__(self, processor):
            if not type(processor.event) == ReactionAdded:
                return

            emojus = processor.event.reaction
            if emojus in self.emoji:
                return self.func(processor)

    return inner_wrapper


class entry_point:
    func_wrapper = basic_func_wrapper

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def __call__(self, processor_class):
        return self.func_wrapper(processor_class)


class messages_only(entry_point):
    func_wrapper = typed_func_wrapper(Message)


class reacts_only(entry_point):
    func_wrapper = typed_func_wrapper(ReactionAdded)


class hello_only(entry_point):
    func_wrapper = typed_func_wrapper(Hello)


class user_change_only(entry_point):
    func_wrapper = typed_func_wrapper(UserChange)


# alias'd for clarity
on_startup = hello_only


class allow_events(entry_point):
    def __init__(self, *args, **kwargs):
        self.func_wrapper = typed_func_wrapper(*args)
        super().__init__(*args, **kwargs)


class no_bots(entry_point):
    func_wrapper = no_bot_func_wrapper
